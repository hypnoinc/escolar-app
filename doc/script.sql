-- MySQL Workbench Synchronization
-- Generated: 2019-10-03 21:08
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: LGpir

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `escolar2` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXISTS `escolar2`.`cidade` (
  `id_cidade` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `id_estado` INT(11) NOT NULL,
  PRIMARY KEY (`id_cidade`),
  INDEX `fk_cidade_estado1_idx` (`id_estado` ASC),
  CONSTRAINT `fk_cidade_estado1`
    FOREIGN KEY (`id_estado`)
    REFERENCES `escolar2`.`estado` (`id_estado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `escolar2`.`escola` (
  `id_escola` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NULL DEFAULT NULL,
  `endereco` VARCHAR(150) NULL DEFAULT NULL,
  `id_cidade` INT(11) NOT NULL,
  PRIMARY KEY (`id_escola`),
  INDEX `fk_escola_cidade1_idx` (`id_cidade` ASC),
  CONSTRAINT `fk_escola_cidade1`
    FOREIGN KEY (`id_cidade`)
    REFERENCES `escolar2`.`cidade` (`id_cidade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `escolar2`.`estado` (
  `id_estado` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `sigla` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id_estado`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `escolar2`.`tipo_usuario` (
  `id_tipo_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_tipo_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `escolar2`.`usuario` (
  `id_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `email` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `senha` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `id_tipo_usuario` INT(11) NOT NULL DEFAULT '1',
  `id_escola` INT(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  INDEX `fk_usuario_tipo_usuario_idx` (`id_tipo_usuario` ASC),
  INDEX `fk_usuario_escola1_idx` (`id_escola` ASC),
  CONSTRAINT `fk_usuario_tipo_usuario`
    FOREIGN KEY (`id_tipo_usuario`)
    REFERENCES `escolar2`.`tipo_usuario` (`id_tipo_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_escola1`
    FOREIGN KEY (`id_escola`)
    REFERENCES `escolar2`.`escola` (`id_escola`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `escolar2`.`ensino` (
  `id_ensino` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NULL DEFAULT NULL,
  PRIMARY KEY (`id_ensino`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `escolar2`.`turma` (
  `id_turma` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NULL DEFAULT NULL,
  `id_ensino_escola` INT(11) NOT NULL,
  `nivel` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_turma`),
  INDEX `fk_turma_ensino_escola1_idx` (`id_ensino_escola` ASC),
  CONSTRAINT `fk_turma_ensino_escola1`
    FOREIGN KEY (`id_ensino_escola`)
    REFERENCES `escolar2`.`ensino_escola` (`id_ensino_escola`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `escolar2`.`ensino_escola` (
  `id_ensino_escola` INT(11) NOT NULL AUTO_INCREMENT,
  `id_ensino` INT(11) NOT NULL,
  `id_escola` INT(11) NOT NULL,
  `id_metodo_avaliacao` INT(11) NOT NULL,
  PRIMARY KEY (`id_ensino_escola`),
  INDEX `fk_ensino_escola_ensino1_idx` (`id_ensino` ASC),
  INDEX `fk_ensino_escola_escola1_idx` (`id_escola` ASC),
  INDEX `fk_ensino_escola_metodo_avaliacao1_idx` (`id_metodo_avaliacao` ASC),
  CONSTRAINT `fk_ensino_escola_ensino1`
    FOREIGN KEY (`id_ensino`)
    REFERENCES `escolar2`.`ensino` (`id_ensino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ensino_escola_escola1`
    FOREIGN KEY (`id_escola`)
    REFERENCES `escolar2`.`escola` (`id_escola`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ensino_escola_metodo_avaliacao1`
    FOREIGN KEY (`id_metodo_avaliacao`)
    REFERENCES `escolar2`.`metodo_avaliacao` (`id_metodo_avaliacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `escolar2`.`metodo_avaliacao` (
  `id_metodo_avaliacao` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NULL DEFAULT NULL,
  `descricao` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`id_metodo_avaliacao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `escolar2`.`aluno_turma` (
  `id_aluno_turma` INT(11) NOT NULL AUTO_INCREMENT,
  `id_turma` INT(11) NOT NULL,
  `id_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`id_aluno_turma`),
  INDEX `fk_aluno_turma_turma1_idx` (`id_turma` ASC),
  INDEX `fk_aluno_turma_usuario1_idx` (`id_usuario` ASC),
  CONSTRAINT `fk_aluno_turma_turma1`
    FOREIGN KEY (`id_turma`)
    REFERENCES `escolar2`.`turma` (`id_turma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aluno_turma_usuario1`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `escolar2`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
