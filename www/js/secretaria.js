
module.controller('secretariaController', function($scope) {

    $scope.turmaList = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.turmas = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.turmaAdd = function() {
        var msg = '';

        if ($scope.data.name == '')
            msg = 'Entre com um nome valido';
        if ($scope.data.sigla == '')
            msg = 'Entre com uma sigla valida';
        if ($scope.data.nivel == '')
            msg = 'Entre com um nivel valido';
        if ($scope.data.serie == '')
            msg = 'Entre com uma série valida';
        if ($scope.data.id_ensino_escola == 0 || $scope.data.id_ensino_escola == '')
            msg = 'Selecione um ensino';

        $scope.data.idEscola = $scope.user.idSchool;

        if (msg != '')
            ons.notification.alert(msg);
        else
            $scope.ajax('turma/salvar',$scope.data ,'turmaRetorno');
    }

    $scope.turmaRetorno = async function(retorno) {
        if (retorno.status == 'ok') {
            await myNavigator.removePage(myNavigator.pages.length-1);
            $scope.replace('secretaria/turma-list.html');
    	} else
            $scope.displayError(retorno);
    };

    $scope.turmaGet = function(data) {
        $scope.ajax('ensino', {idEscola: $scope.user.idSchool},'ensinoLoad');
        if (data && 'id_turma' in data)
            $scope.ajax('turma', {idEscola: $scope.user.idSchool, id: data.id_turma},'turmaLoad');
    }

    $scope.turmaLoad = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.data = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.ensinoLoad = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.ensino = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.turmaDelete = async function() {
        var retorno = await ons.notification.confirm(
            'Tem certeza que deseja deletar este Registro?',
            { buttonLabels: ['<span class="font-vermelha">Cancelar</span>',
                             '<span class="font-verde">Confirmar</span>'] }
        );
        if(retorno){
            $scope.ajax('turma/excluir', {id: $scope.data.id_turma}, 'turmaRetorno');
        }
    }


    // Funções referente ao cadastro de nota //


    $scope.notaGet = function(data) {
        $scope.periodos = ['','1º tri','2º tri','3º tri','1º tri Rec','2º tri Rec','3º tri Rec']
        $scope.data = {id_aluno_turma: myNavigator.topPage.data.id_aluno_turma,periodo: '', id_disciplina: '', id_area: ''};
        $scope.ajax('nota', {idEscola: $scope.user.idSchool, idAlunoTurma: data.id_aluno_turma,},'notasLoad');
        $scope.ajax('disciplina', {idEscola: $scope.user.idSchool, idTurma: data.id_turma,},'disciplinaLoadNota');
        if (data && 'id' in data)
            $scope.ajax('nota', {idEscola: $scope.user.idSchool, id: data.id},'notaLoad');
    }

    $scope.disciplinaLoadNota = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.disciplina = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.notaLoad = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.data = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.notasLoad = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.notas = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.notaAdd = function() {
        var msg = '';

        if ($scope.data.periodo == '')
            msg = 'Entre com um periodo valido';
        if ($scope.data.id_disciplina == '')
            msg = 'Entre com uma disciplina valida';
        if ($scope.data.valor == '')
            msg = 'Entre com um nota valida';

        $scope.data.idEscola = $scope.user.idSchool;

        Disco.set('dados_temporarios',myNavigator.topPage.data);

        if (msg != '')
            ons.notification.alert(msg);
        else
            $scope.ajax('nota/salvar',$scope.data ,'notaRetorno');
    }

    $scope.notaRetorno = async function(retorno) {
        if (retorno.status == 'ok') {
            await myNavigator.removePage(myNavigator.pages.length-1);
            var dados = Disco.get('dados_temporarios');
            Disco.set('dados_temporarios', {});
            $scope.replace('secretaria/nota-list.html', dados);
    	} else
            $scope.displayError(retorno);
    };

    $scope.notaEditar = async function(data, id) {
        var dados = {
            id: id,
            id_aluno_turma: data.id_aluno_turma,
            aluno: data.aluno
        }
        $scope.load('secretaria/nota-reg.html', dados);
    };

    $scope.notaDelete = async function() {
        var retorno = await ons.notification.confirm(
            'Tem certeza que deseja deletar este Registro?',
            { buttonLabels: ['<span class="font-vermelha">Cancelar</span>',
                             '<span class="font-verde">Confirmar</span>'] }
        );
        if(retorno){
            $scope.ajax('nota/excluir', {id: $scope.data.id_nota}, 'notaRetorno');
        }
    }


    //  Funções do cadastro de alunos //


    $scope.alunoGet = function(data) {
        if (data && 'id' in data)
            $scope.ajax('usuario', {idEscola: $scope.user.idSchool, id: data.id},'alunoLoad');
    }

    $scope.alunoLoad = function(retorno) {                                                     //todo fazer uma função data load
        if (retorno.status == 'ok'){
            $scope.data = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.alunoAdd = function() {
        var msg = '';

        if ($scope.data.nome == '')
            msg = 'Entre com um nome valido';

        $scope.data.idEscola = $scope.user.idSchool;
        $scope.data.idTurma  = myNavigator.topPage.data.id_turma;

        Disco.set('dados_temporarios',myNavigator.topPage.data);

        if (msg != '')
            ons.notification.alert(msg);
        else
            $scope.ajax('aluno/salvar',$scope.data ,'alunoRetorno');
    }

    $scope.alunoRetorno = async function(retorno) {
        if (retorno.status == 'ok') {
            await myNavigator.removePage(myNavigator.pages.length-1);
            var dados = Disco.get('dados_temporarios');
            Disco.set('dados_temporarios', {});
            $scope.replace('secretaria/aluno-list.html', dados);
    	} else
            $scope.displayError(retorno);
    };

    $scope.alunoDelete = async function() {
        var retorno = await ons.notification.confirm(
            'Tem certeza que deseja deletar este Registro?',
            { buttonLabels: ['<span class="font-vermelha">Cancelar</span>',
                             '<span class="font-verde">Confirmar</span>'] }
        );
        if(retorno){
            $scope.ajax('aluno/excluir', {id: $scope.data.id_usuario}, 'alunoRetorno');
        }
    }

    $scope.alunoGetSenha = function(id) {
        $scope.ajax('aluno/senha', {id: id},'alunoSenhaView');
    }

    $scope.alunoSenhaView = function(retorno) {
        if (retorno.status == 'ok')
            ons.notification.alert('Senha: '+retorno.senha);
    	else
            $scope.displayError(retorno);
    };


        //  Funções do cadastro de Disciplinas //


    $scope.disciplinaList = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.disciplinas = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.disciplinaCor = function(id, cor) {
        document.getElementById(id).style.backgroundColor = cor;
    };

    $scope.disciplinaGet = function(data) {
        if (data && 'id_disciplina' in data)
            $scope.ajax('disciplina', {idEscola: $scope.user.idSchool, id_disciplina: data.id_disciplina},'disciplinaLoad');
    }

    $scope.disciplinaLoad = function(retorno) {                                                     //todo fazer uma função data load
        if (retorno.status == 'ok'){
            $scope.data = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.disciplinaAdd = function() {
        var msg = '';

        if ($scope.data.name == '')
            msg = 'Entre com um nome valido';
        if ($scope.data.sigla == '')
            msg = 'Entre com uma sigla valida';
        if ($scope.data.cor == '')
            msg = 'Entre com uma cor valida';

        $scope.data.idEscola = $scope.user.idSchool;

        if (msg != '')
            ons.notification.alert(msg);
        else
            $scope.ajax('disciplina/salvar',$scope.data ,'disciplinaRetorno');
    }

    $scope.disciplinaRetorno = async function(retorno) {
        if (retorno.status == 'ok') {
            await myNavigator.removePage(myNavigator.pages.length-1);
            $scope.replace('secretaria/disciplina-list.html');
    	} else
            $scope.displayError(retorno);
    };

    $scope.disciplinaDelete = async function() {
        var retorno = await ons.notification.confirm(
            'Tem certeza que deseja deletar este Registro?',
            { buttonLabels: ['<span class="font-vermelha">Cancelar</span>',
                             '<span class="font-verde">Confirmar</span>'] }
        );
        if(retorno){
            $scope.ajax('disciplina/excluir', {id: $scope.data.id_disciplina}, 'disciplinaRetorno');
        }
    }



    // Funções referente ao Horario


    $scope.horarioGet = function(data) {
        $scope.data = {
            dia_semana: '',
            periodo: '',
            id_disciplina: '',
            id_professor: ''
        };
        //$scope.ajax('disciplina', {idEscola: $scope.user.idSchool},'disciplinaLoad');
    }

});
