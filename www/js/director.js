
module.controller('directorController', function($scope) {

    $scope.funcionariosList = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.funcionarios = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.funcionarioAdd = function() {
        var msg = '';

        if ($scope.data.nome == '')
            msg = 'Entre com um nome valido';
        if ($scope.data.email == '')
            msg = 'Entre com um Email valido';
        if ($scope.data.senha == '')
            msg = 'Entre com uma Senha valida';
        if ($scope.data.id_tipo_usuario == 0 || $scope.data.id_tipo_usuario == '')
            msg = 'Selecione um Cargo';

        $scope.data.idEscola = $scope.user.idSchool;

        if (msg != '')
            ons.notification.alert(msg);
        else
            $scope.ajax('usuario/salvar',$scope.data ,'funcionarioReturn');
    }

    $scope.funcionarioReturn = async function(retorno) {
        if (retorno.status == 'ok') {
            await myNavigator.removePage(myNavigator.pages.length-1);
            $scope.replace('director/funcionario-list.html');
    	} else
            $scope.displayError(retorno);
    };

    $scope.funcionarioGet = function(data) {
        if (data && 'id_usuario' in data)
            $scope.ajax('usuario', {idEscola: $scope.user.idSchool, id: data.id_usuario},'funcionarioLoad');
    }

    $scope.funcionarioLoad = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.data = retorno.resultado;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.funcionarioDelete = async function() {
        var retorno = await ons.notification.confirm(
            'Tem certeza que deseja deletar este Registro?',
            { buttonLabels: ['<span class="font-vermelha">Cancelar</span>',
                             '<span class="font-verde">Confirmar</span>'] }
        );
        if(retorno){
            $scope.ajax('usuario/excluir', {id: $scope.data.id_usuario}, 'funcionarioReturn');
        }
    }

});
