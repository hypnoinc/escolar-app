ons.platform.select('android');
var module = ons.bootstrap();
module.controller('escolarController', function($scope) {

    $scope.data   = null;
    $scope.search = '';
    $scope.metaData = document.data;

    $scope.init = function() {
        $scope.divLoad = document.getElementById('divLoad');
        $scope.user = Disco.get('user');
        if ($scope.user)
            $scope.login = true;
        else
            $scope.login = false;
    };

    $scope.ajax = function(target, parameter, funcCallBack) {

        //API Microservice
        //var urlServer = "http://localhost:8080/"+target;
        //var urlServer = "http://ec2-54-233-192-31.sa-east-1.compute.amazonaws.com/"+target;

        //API FaaS
        var urlServer = "https://cr5ov2f6jd.execute-api.sa-east-1.amazonaws.com/Teste/"+target;

        $scope.divLoad.style.display = 'inline';

        if(!parameter)
            parameter = 0;

        var request = new XMLHttpRequest();
        request.open('POST', urlServer);
        request.setRequestHeader("Content-Type", "application/json");
        request.setRequestHeader("x-api-key", "gYrgcN1HbD8mjnUV4FQYX4ifjtZB5vyX8r6wf2Sh");
        request.onreadystatechange = () => {
        	if (request.readyState == 4){
                $scope.divLoad.style.display = 'none';
				if(request.status == 200){
					if(typeof this[funcCallBack] == 'function')
                        if ($scope.IsJsonString(request.responseText))
	            		    this[funcCallBack](JSON.parse(request.responseText));
                        else
                            this[funcCallBack]({err: 'responseText Fail'});
				} else
                    console.log(1, 'Falha na Requisição');
        	}
		}
        request.onerror = err => {
            console.log(2, 'Err A02 ', err);
            $scope.divLoad.style.display = 'none';
        }

        request.send(JSON.stringify(parameter));
    };

    $scope.IsJsonString = function(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    $scope.loginReturn = function(retorno) {
        if (retorno.status == 'ok') {
            $scope.login = true;
            var user = {'status':   'ok',
                        'idUser':   retorno.resultado.id_usuario,
                        'name':     retorno.resultado.nome,
                        'idSchool': retorno.resultado.id_escola,
                        'userType': 2};
            $scope.user = user;
    		Disco.set('user', user);
    		Disco.set('logado', true);
            myNavigator.resetToPage('views/home.html');
            $scope.$digest();
    	} else {
            $scope.displayError(retorno);
    	}
    };

    $scope.logout = function() {
        Disco.set('user', false);
        myNavigator.resetToPage('views/login.html');
        $scope.logado = false;
        Disco.set('logado', false);
    };

    $scope.load = function(page, data){
        myNavigator.pushPage('views/'+page, {data: data});
    };

    $scope.replace = async function(page, data) {
        await myNavigator.removePage(myNavigator.pages.length-1,{animation: 'none'});
        myNavigator.pushPage('views/'+page,{data: data,animation: 'none'});
    };

    $scope.dataClear  = function(){
        $scope.data   = null;
        $scope.search = '';
    }

    $scope.displayError = function(retorno){
        if(retorno.msg)
            ons.notification.alert(retorno.msg);
        else
            ons.notification.alert('Erro ao se comunicar com o servidor');
    }
});
